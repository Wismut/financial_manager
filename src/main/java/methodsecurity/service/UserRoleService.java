package methodsecurity.service;

import methodsecurity.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

@Service
public class UserRoleService {
    @Autowired
    UserRoleRepository userRoleRepository;

    @Secured({"ROLE_VIEWER", "ROLE_EDITOR"})
    public boolean isValidUsername(String username) {
        return userRoleRepository.isValidUsername(username);
    }
}
