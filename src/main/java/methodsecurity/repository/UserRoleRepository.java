package methodsecurity.repository;

import methodsecurity.entity.CustomerUser;

import java.util.Map;

public class UserRoleRepository {
    private Map<String, CustomerUser> DB_BASED_USER_MAPPING;

    public boolean isValidUsername(String username) {
        return DB_BASED_USER_MAPPING.containsKey(username);
    }
}
