package model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Account {
    private int id;
    private String description;
    private double balance;
    private Set<Record> recordSet = new HashSet<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Set<Record> getRecordSet() {
        return recordSet;
    }

    public void setRecordSet(Set<Record> recordSet) {
        this.recordSet = recordSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
