import java.util.UUID;

public class Utils {
    public static String getUniqueString() {
        return UUID.randomUUID().toString();
    }
}
