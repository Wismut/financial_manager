import model.Account;
import model.Record;
import model.User;


import java.util.HashSet;
import java.util.Set;

public class DataStoreImpl implements DataStore {
    private Set<User> userSet = new HashSet<>();

    public User getUser(String name) {
        for (User user : userSet) {
            if (user.getLogin().equals(name)) {
                return user;
            }
        }
        return null;
    }

    public Set<String> getUserNames() {
        Set<String> userNames = new HashSet<>();
        userSet.forEach((user) -> userNames.add(user.getLogin()));
        return userNames;
    }

    public Set<Account> getAccounts(User owner) {
        return owner.getAccountSet();
    }

    public Set<Record> getRecords(Account account) {
        return account.getRecordSet();
    }

    public void addUser(User user) {
        userSet.add(user);
    }

    public void addAccount(User user, Account account) {
        user.getAccountSet().add(account);
    }

    public void addRecord(Account account, Record record) {
        account.getRecordSet().add(record);
    }

    public User removeUser(String name) {
        for (User currentUser : userSet) {
            if (currentUser.getLogin().equals(name)) {
                userSet.remove(currentUser);
                return currentUser;
            }
        }
        return null;
    }

    public Account removeAccount(User owner, Account account) {
        owner.getAccountSet().remove(account);
        return account;
    }

    public Record removeRecord(Account from, Record record) {
        from.getRecordSet().remove(record);
        return record;
    }
}
